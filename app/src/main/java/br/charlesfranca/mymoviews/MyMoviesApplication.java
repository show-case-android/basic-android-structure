package br.charlesfranca.mymoviews;

import android.app.Application;

import dependency_injection.DaggerMyMoviesComponent;
import dependency_injection.MyMoviesComponent;
import services.api.ServiceGenerator;
import services.interfaces.IMovieService;

public class MyMoviesApplication extends Application {
    private static MyMoviesComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        initDagger();
    }

    private void initDagger() {
        component = DaggerMyMoviesComponent.builder().build();

    }

    public static MyMoviesComponent getComponent() {
        return component;
    }
}
