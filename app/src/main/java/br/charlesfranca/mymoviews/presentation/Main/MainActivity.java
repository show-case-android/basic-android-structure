package br.charlesfranca.mymoviews.presentation.Main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.charlesfranca.mymoviews.MyMoviesApplication;
import br.charlesfranca.mymoviews.R;
import br.charlesfranca.mymoviews.presentation.Main.adapters.MovieSectionAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import models.Movie;
import services.api.ApiResponse;
import services.implementation.MovieService;

public class MainActivity extends AppCompatActivity {
    @Inject
    MovieService movieService;

    @BindView(R.id.recPopular)
    RecyclerView recPopular;

    @BindView(R.id.recEmCartaz)
    RecyclerView topRated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        MyMoviesApplication.getComponent().inject(this);

        LoadPopularMovies();
        LoadNowPlaying();
    }

    protected void LoadPopularMovies() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recPopular.setLayoutManager(layoutManager);

        final ArrayList<Movie> popularMovies = new ArrayList<Movie>();
        MovieSectionAdapter adapter = new MovieSectionAdapter(this, popularMovies, "Popular");
        adapter.setClickListener(new MovieSectionAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });
        recPopular.setAdapter(adapter);

        movieService.getPopular(new ApiResponse<List<Movie>>() {
            @Override
            public void success(List<Movie> data) {
                List<Movie> movies = data;
                popularMovies.clear();
                popularMovies.addAll(movies);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void error(Error error) {
                Error e = error;
            }
        });
    }

    protected void LoadNowPlaying() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        topRated.setLayoutManager(layoutManager);

        final ArrayList<Movie> nowPlayingMovies = new ArrayList<Movie>();
        MovieSectionAdapter adapter = new MovieSectionAdapter(this, nowPlayingMovies, "Popular");
        adapter.setClickListener(new MovieSectionAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });
        topRated.setAdapter(adapter);

        movieService.getNowPlaying(new ApiResponse<List<Movie>>() {
            @Override
            public void success(List<Movie> data) {
                List<Movie> movies = data;
                nowPlayingMovies.clear();
                nowPlayingMovies.addAll(movies);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void error(Error error) {
                Error e = error;
            }
        });
    }
}
