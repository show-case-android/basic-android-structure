package br.charlesfranca.mymoviews.presentation.Main.adapters;

import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import br.charlesfranca.mymoviews.R;
import models.Movie;

public class MovieSectionAdapter extends RecyclerView.Adapter<MovieSectionAdapter.ViewHolder> {

    private ArrayList<Movie> movies;
    private String title;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public MovieSectionAdapter(Context context, ArrayList<Movie> movies, String title) {
        this.mInflater = LayoutInflater.from(context);
        this.movies = movies;
        this.title = title;
    }

    // inflates the row layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.section_layout, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the view and textview in each row
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Movie movie = movies.get(position);
        TextView txtInfo = holder.myView.findViewById(R.id.info_text);
        txtInfo.setText(movie.getTitle());
        ImageView img = holder.myView.findViewById(R.id.movieImage);
        Picasso.get().load("https://image.tmdb.org/t/p/w500" + movie.getPosterPath()).into(img);

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return movies.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        View myView;
        ViewHolder(View itemView) {
            super(itemView);
            myView = itemView.findViewById(R.id.MovieSectionItem);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public Movie getItem(int id) {
        return movies.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}