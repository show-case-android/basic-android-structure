package dependency_injection;

import javax.inject.Singleton;

import br.charlesfranca.mymoviews.presentation.Main.MainActivity;
import dagger.Component;

@Singleton
@Component(modules = {MyMoviesModule.class})
public interface MyMoviesComponent {
    public void inject(MainActivity activity);
}