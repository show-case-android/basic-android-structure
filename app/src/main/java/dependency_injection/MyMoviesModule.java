package dependency_injection;

import dagger.Module;
import dagger.Provides;
import services.implementation.MovieService;

@Module
public class MyMoviesModule {
    @Provides
    public MovieService movieService() {
        return new MovieService();
    }
}
