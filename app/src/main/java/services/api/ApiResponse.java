package services.api;

public interface ApiResponse<T> {
    public void success(T data);
    public void error(Error error);
}
