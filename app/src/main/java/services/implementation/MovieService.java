package services.implementation;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import java.util.List;

import models.Movie;
import services.api.ApiResponse;
import services.api.IApiListResponse;
import services.api.ServiceGenerator;
import services.interfaces.IMovieService;

public class MovieService {
    IMovieService movieService;
    public MovieService() {
        movieService = ServiceGenerator.createService(IMovieService.class);
    }

    public void getPopular(ApiResponse<List<Movie>> callback) {
        Call<IApiListResponse<Movie>> call = movieService.getPopular();
        call.enqueue(new Callback<IApiListResponse<Movie>>() {
            @Override
            public void onResponse(Call<IApiListResponse<Movie>> call, Response<IApiListResponse<Movie>> response) {
                if(response.isSuccessful()) {
                    callback.success(response.body().getResults());
                }else{
                    Error error = new Error(response.message());
                    callback.error(error);
                }
            }

            @Override
            public void onFailure(Call<IApiListResponse<Movie>> call, Throwable t) {
                Error error = new Error(t.getMessage());
            }
        });
    }

    public void getNowPlaying(ApiResponse<List<Movie>> callback) {
        Call<IApiListResponse<Movie>> call = movieService.getNowPlaying();
        call.enqueue(new Callback<IApiListResponse<Movie>>() {
            @Override
            public void onResponse(Call<IApiListResponse<Movie>> call, Response<IApiListResponse<Movie>> response) {
                if(response.isSuccessful()) {
                    callback.success(response.body().getResults());
                }else{
                    Error error = new Error(response.message());
                    callback.error(error);
                }
            }

            @Override
            public void onFailure(Call<IApiListResponse<Movie>> call, Throwable t) {
                Error error = new Error(t.getMessage());
            }
        });
    }
}
