package services.interfaces;

import java.util.List;

import models.Movie;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import services.api.IApiListResponse;

public interface IMovieService {
    @GET("movie/{id}")
    Call<List<Movie>> getMovie(@Path("id") int id);

    @GET("movie/popular")
    Call<IApiListResponse<Movie>> getPopular();

    @GET("movie/top_rated")
    Call<IApiListResponse<Movie>> getNowPlaying();
}
